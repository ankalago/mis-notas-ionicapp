(function(){
  var app = angular.module('starter', ['ionic','starter.notaStore']);

  app.config(['$stateProvider','$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('list',{
      url : '/list',
      templateUrl : 'templates/list.html'
    });

    $stateProvider.state('edit',{
      url : '/edit/:id',
      controller: 'editCtrl',
      templateUrl : 'templates/edit.html'
    });

    $stateProvider.state('create',{
      url : '/create',
      controller: 'createCtrl',
      templateUrl : 'templates/edit.html'
    });

    $urlRouterProvider.otherwise('/list');
  }]);

  app.controller('listCtrl', ['$scope','notaStore', function ($scope, notaStore) {
    $scope.notas = notaStore.list();
    $scope.remove = function(id){
      notaStore.remove(id);
    }
  }]);

  app.controller('editCtrl', ['$scope','$state','notaStore', function ($scope, $state, notaStore) {
    $scope.id = $state.params.id;
    $scope.nota = angular.copy(notaStore.get($scope.id));
    $scope.save = function(){
      notaStore.update($scope.nota);
      $state.go('list');
    }
  }]);

  app.controller('createCtrl', ['$scope','$state','notaStore', function ($scope, $state, notaStore) {
    $scope.nota = {id:new Date().getTime().toString(), titulo:'', descripcion:''};
    $scope.save = function(){
      notaStore.create($scope.nota);
      $state.go('list');
    }
  }]);

  app.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if(window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  });
}());